---
layout: post
title:  "Hello jekyll"
date:   2020-03-24 15:32:14 -0600
categories: jekyll update
---
Test post
{% highlight ruby %}
def print_hi(name)
  puts "Hi, #{name}"
end
print_hi('Tom')
#=> prints 'Hi, Tom' to STDOUT.
{% endhighlight %}

## Test mathjax

this is inline math $\displaystyle \int_{0}^{1}f(x)dx$ display right?

following block math

$$
\sum_{n=0}^{+\infty}a_{n}+f(x)=g(x)
$$